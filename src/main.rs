#[macro_use]
extern crate log;

use clokwerk::{AsyncScheduler, TimeUnits};
use env_logger::Env;
use lettre::{
    message::SinglePart,
    transport::smtp::{
        authentication::{Credentials, Mechanism},
        client::{Tls, TlsParameters, TlsVersion},
        SMTP_PORT,
    },
    Message, SmtpTransport, Transport,
};
use log::Level;
use serde::{Deserialize, Deserializer, Serialize};
use std::collections::HashMap;
use std::fmt;
use std::fs::{write, File};
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::Mutex;

#[derive(Deserialize)]
struct Wrapper {
    response: Vec<Ad>,
}

#[allow(non_snake_case)]
#[derive(Deserialize, Serialize)]
struct Ad {
    _id: i32,
    title: String,
    HFDist: f64,
    #[serde(deserialize_with = "optional_string_or_number")]
    doubleBed: Option<String>,
    infos: Option<String>,
    #[serde(deserialize_with = "optional_string_or_number")]
    nbPerson: Option<String>,
    #[serde(deserialize_with = "optional_string_or_number")]
    simpleBed: Option<String>,
    r#type: String,
    #[serde(deserialize_with = "optional_string_or_number")]
    tel: Option<String>,
    email: String,
}

impl fmt::Display for Ad {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        fn adapt(o: &Option<String>) -> String {
            o.as_ref().unwrap_or(&"-".to_string()).to_string()
        }

        fmt::Display::fmt(
            format!(
                "#{} {}
  - type : {}
  - distance : {}
  - nombre de personnes : {}
  - lits doubles : {}
  - lits simples : {}
  - infos : {}
  - email : {}
  - tel : {}",
                self._id,
                self.title,
                self.r#type,
                self.HFDist,
                adapt(&self.nbPerson),
                adapt(&self.doubleBed),
                adapt(&self.simpleBed),
                adapt(&self.infos),
                self.email,
                adapt(&self.tel)
            )
            .as_str(),
            f,
        )
    }
}

const DOMAIN: &str = "example.com";
const FILE: &str = "./ads.json";
const FROM: &str = "hfh-notifier@example.com";
const MAX_DISTANCE: f64 = 2000f64;
const TO: &[&str] = &["me@example.com", "myself@example.com", "I@example.com"];
const URL: &str = "https://hellfest-hebergement.fr/api/index.php?newAPI=1";

#[tokio::main]
async fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or(format!("{}", Level::Info)))
        .init();

    info!("Starting HFH-notifier");

    let runner = Arc::new(Mutex::new(Runner::default()));

    let mut scheduler = AsyncScheduler::new();
    scheduler.every(1.hour()).run(move || {
        let runner = runner.clone();
        async move {
            if let Err(e) = runner.lock().await.run().await {
                error!("{}", e);
            }
        }
    });

    loop {
        scheduler.run_pending().await;
        tokio::time::sleep(Duration::from_millis(100)).await;
    }
}

struct Runner<'a> {
    client: reqwest::Client,
    params: HashMap<&'a str, &'a str>,
    seen: HashMap<i32, Ad>,
    mailer: SmtpTransport,
}

impl Runner<'_> {
    async fn run(&mut self) -> Result<(), RunnerError> {
        info!("Fetching ads…");
        let wrapper: Wrapper = self
            .client
            .post(URL)
            .form(&self.params)
            .timeout(Duration::from_secs(30))
            .send()
            .await?
            .json()
            .await?;
        info!("Fetched {} ads", wrapper.response.len());
        let interesting: Vec<Ad> = wrapper
            .response
            .into_iter()
            .filter(|ad| ad.HFDist < MAX_DISTANCE && !self.seen.contains_key(&ad._id))
            .collect();
        if interesting.is_empty() {
            info!("No new interesting ads");
        } else {
            info!("{} new interesting ads", interesting.len());
            self.notify(&interesting)?;
            for ad in interesting {
                info!("New interesting ad: {}", ad);
                self.seen.insert(ad._id, ad);
            }
            match serde_json::to_string(&self.seen).map(|s| write(FILE, s)) {
                Err(e) => error!("Could not dump ads: {}", e),
                _ => info!("Updated {} with new ads", FILE),
            }
        }
        Ok(())
    }

    fn notify(&self, ads: &[Ad]) -> Result<(), RunnerError> {
        let email = TO
            .iter()
            .fold(
                Ok(Message::builder()
                    .from(FROM.parse()?)
                    .subject(format!("{} hébergement(s) Hellfest à vérifier", ads.len()))),
                |builder, &to| builder.and_then(|b| to.parse().map(|t| b.to(t))),
            )?
            .singlepart(SinglePart::plain(
                ads.iter()
                    .map(|ad| ad.to_string())
                    .collect::<Vec<String>>()
                    .join("\n\n---\n\n"),
            ))?;

        self.mailer.send(&email)?;
        info!("Notification sent");
        Ok(())
    }
}

impl Default for Runner<'_> {
    fn default() -> Self {
        let client = reqwest::Client::new();

        let mut params = HashMap::new();
        params.insert("action", "annonces");

        let seen: HashMap<i32, Ad> = match File::open(FILE) {
            Ok(f) => match serde_json::from_reader(f) {
                Ok(m) => Some(m),
                Err(e) => {
                    error!(
                        "Could not deserialize already-seen ads from {}: {}",
                        FILE, e
                    );
                    None
                }
            },
            Err(e) => {
                warn!("Could not open already-seen ads from {}: {}", FILE, e);
                None
            }
        }
        .unwrap_or_default();
        info!("{} already-seen ads", seen.len());

        let tls_parameters = TlsParameters::builder(DOMAIN.to_owned())
            .set_min_tls_version(TlsVersion::Tlsv12)
            .build()
            .unwrap();

        let mailer = SmtpTransport::builder_dangerous(DOMAIN)
            .tls(Tls::Required(tls_parameters))
            .port(SMTP_PORT)
            .credentials(Credentials::new("user".to_string(), "password".to_string()))
            .authentication(vec![Mechanism::Plain])
            .build();

        Self {
            client,
            params,
            seen,
            mailer,
        }
    }
}

#[derive(Debug, thiserror::Error)]
enum RunnerError {
    #[error("Could not fetch ads: {0}")]
    Fetch(#[from] reqwest::Error),
    #[error(transparent)]
    Address(#[from] lettre::address::AddressError),
    #[error(transparent)]
    Email(#[from] lettre::error::Error),
    #[error(transparent)]
    Smtp(#[from] lettre::transport::smtp::Error),
}

fn optional_string_or_number<'de, D>(deserializer: D) -> Result<Option<String>, D::Error>
where
    D: Deserializer<'de>,
{
    #[derive(Deserialize)]
    #[serde(untagged)]
    enum UntypedAPI {
        String(String),
        Number(i64),
        Null,
    }

    match UntypedAPI::deserialize(deserializer)? {
        UntypedAPI::String(s) => Ok(Some(s)),
        UntypedAPI::Number(i) => Ok(Some(format!("{}", i))),
        UntypedAPI::Null => Ok(None),
    }
}
